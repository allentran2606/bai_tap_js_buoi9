function getInputFromForm() {
  let account = document.getElementById(`tknv`).value;
  let name = document.getElementById(`name`).value;
  let email = document.getElementById(`email`).value;
  let password = document.getElementById(`password`).value;
  let datepicker = document.getElementById(`datepicker`).value;
  let incomeCB = document.getElementById(`luongCB`).value;
  let position = document.getElementById(`chucvu`).value;
  let workingTime = document.getElementById(`gioLam`).value;

  var nv = new NhanVien(
    account,
    name,
    email,
    password,
    datepicker,
    incomeCB,
    position,
    workingTime
  );
  return nv;
}

function showOuput(index) {
  document.getElementById(`tknv`).value = index.account;
  document.getElementById(`name`).value = index.name;
  document.getElementById(`email`).value = index.email;
  document.getElementById(`password`).value = index.passWord;
  document.getElementById(`datepicker`).value = index.date;
  document.getElementById(`luongCB`).value = index.incomeCB;
  document.getElementById(`chucvu`).value = index.position;
  document.getElementById(`gioLam`).value = index.time;
}

function renderDsnv(nvArr) {
  let contentHTML = ``;
  let obj = getInputFromForm();
  nvArr.forEach((nv) => {
    contentHTML += `
    <tr>
      <td>${nv.account}</td>
      <td>${nv.name}</td>
      <td>${nv.email}</td>
      <td>${nv.date}</td>
      <td>${nv.position}</td>
      <td>${nv.totalIncome()}</td>
      <td>${nv.ranking()}</td>
      <td>
        <button class="btn btn-danger" onclick="deleteUser('${
          nv.account
        }')">Xoá</button>
          <button class="btn btn-warning" onclick="editUser(${
            nv.account
          })" data-toggle="modal"
      data-target="#myModal">Sửa</button>
      </td>
    </tr>
    `;
  });
  document.getElementById(`tableDanhSach`).innerHTML = contentHTML;
}

function findIndex(acc, nvArr) {
  for (let i = 0; i < nvArr.length; i++) {
    let item = nvArr[i];
    if (item.account == acc) {
      return i;
    }
  }
  return -1;
}

function showMessage(idError, message) {
  document.getElementById(idError).innerHTML = message;
  document.getElementById(idError).style.display = `block`;
}

function checkValidation(nv) {
  let count = 0;
  // kiểm tra tài khoản
  if (
    checkEmptyInput(nv.account, `tbTKNV`, `Không được để trống phần này`) &&
    checkAccount(
      nv.account,
      `tbTKNV`,
      `Tài khoản nhân viên phải từ 4-6 ký tự số`
    ) &&
    checkNumber(nv.account, `tbTKNV`, `Tài khoản phải là ký tự số`) &&
    checkSimilarInput(nv, dsnv)
  ) {
    count++;
  }

  // Kiểm tra tên nhân viên
  if (
    checkEmptyInput(nv.name, `tbTen`, `Không được để trống phần này`) &&
    checkName(nv.name, `tbTen`, `Tên nhân viên phải là chữ`)
  ) {
    count++;
  }

  // Kiểm tra email
  if (
    checkEmptyInput(nv.email, `tbEmail`, `Không được để trống phần này`) &&
    checkEmail(nv.email, `tbEmail`, `Email không hợp lệ`)
  ) {
    count++;
  }

  //Kiểm tra mật khẩu
  if (
    checkEmptyInput(nv.passWord, `tbMatKhau`, `Không được để trống phần này`) &&
    checkPassword(
      nv.passWord,
      `tbMatKhau`,
      `Mật khẩu phải chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt`
    )
  ) {
    count++;
  }

  // Kiểm tra ngày tháng
  if (
    checkEmptyInput(nv.date, `tbNgay`, `Không được để trống phần này`) &&
    checkDate(nv.date, `tbNgay`, `Ngày làm không hợp lệ`)
  ) {
    count++;
  }

  // Kiểm tra lương
  if (
    checkEmptyInput(nv.incomeCB, `tbLuongCB`, `Không được để trống phần này`) &&
    checkIncome(nv.incomeCB, `tbLuongCB`, `Lương cơ bản không hợp lệ`)
  ) {
    count++;
  }

  // Kiểm tra chức vụ
  if (
    checkEmptyInput(nv.position, `tbChucVu`, `Không được để trống phần này`) &&
    checkPosition(nv.position, `tbChucVu`, `Vui lòng chọn chức vụ hợp lệ`)
  ) {
    count++;
  }

  // Kiểm tra giờ làm
  if (
    checkEmptyInput(nv.time, `tbGiolam`, `Không được để trống phần này`) &&
    checkHour(nv.time, `tbGiolam`, `Số giờ làm không hợp lệ`)
  ) {
    count++;
  }
  return count;
}

function checkWhenUpdateUser(nv) {
  let count = 0;

  // Kiểm tra tên nhân viên
  if (
    checkEmptyInput(nv.name, `tbTen`, `Không được để trống phần này`) &&
    checkName(nv.name, `tbTen`, `Tên nhân viên phải là chữ`)
  ) {
    count++;
  }

  // Kiểm tra email
  if (
    checkEmptyInput(nv.email, `tbEmail`, `Không được để trống phần này`) &&
    checkEmail(nv.email, `tbEmail`, `Email không hợp lệ`)
  ) {
    count++;
  }

  //Kiểm tra mật khẩu
  if (
    checkEmptyInput(nv.passWord, `tbMatKhau`, `Không được để trống phần này`) &&
    checkPassword(
      nv.passWord,
      `tbMatKhau`,
      `Mật khẩu phải chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt`
    )
  ) {
    count++;
  }

  // Kiểm tra ngày tháng
  if (
    checkEmptyInput(nv.date, `tbNgay`, `Không được để trống phần này`) &&
    checkDate(nv.date, `tbNgay`, `Ngày làm không hợp lệ`)
  ) {
    count++;
  }

  // Kiểm tra lương
  if (
    checkEmptyInput(nv.incomeCB, `tbLuongCB`, `Không được để trống phần này`) &&
    checkIncome(nv.incomeCB, `tbLuongCB`, `Lương cơ bản không hợp lệ`)
  ) {
    count++;
  }

  // Kiểm tra chức vụ
  if (
    checkEmptyInput(nv.position, `tbChucVu`, `Không được để trống phần này`) &&
    checkPosition(nv.position, `tbChucVu`, `Vui lòng chọn chức vụ hợp lệ`)
  ) {
    count++;
  }

  // Kiểm tra giờ làm
  if (
    checkEmptyInput(nv.time, `tbGiolam`, `Không được để trống phần này`) &&
    checkHour(nv.time, `tbGiolam`, `Số giờ làm không hợp lệ`)
  ) {
    count++;
  }
  return count;
}
