function checkEmptyInput(acc, idError, message) {
  if (acc.length == 0) {
    showMessage(idError, message);
    return false;
  } else {
    showMessage(idError, ``);
    return true;
  }
}

function checkSimilarInput(nv, dsnv) {
  let index = findIndex(nv.account, dsnv);
  if (index !== -1) {
    showMessage(`tbTKNV`, `Tài khoản nhân viên đã tồn tại`);
    return false;
  } else {
    showMessage(`tbTKNV`, ``);
    return true;
  }
}

function checkAccount(acc, idError, message) {
  if (acc.length >= 4 && acc.length <= 6) {
    showMessage(idError, ``);
    return true;
  } else {
    showMessage(idError, message);
    return false;
  }
}

function checkNumber(acc, idError, message) {
  let number = /^[0-9]+$/;
  if (number.test(acc)) {
    showMessage(idError, ``);
    return true;
  } else {
    showMessage(idError, message);
    return false;
  }
}

function checkName(name, idError, message) {
  let text =
    /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
  if (text.test(name)) {
    showMessage(idError, ``);
    return true;
  } else {
    showMessage(idError, message);
    return false;
  }
}

function checkEmail(email, idError, message) {
  //   let email =
  //     /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\ [[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  let regexEmail =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (regexEmail.test(email)) {
    showMessage(idError, ``);
    return true;
  } else {
    showMessage(idError, message);
    return false;
  }
}

function checkPassword(pass, idError, message) {
  let regexPassword =
    /^(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{6,10}$/;
  // Link reference: https://stackoverflow.com/questions/19605150/regex-for-password-must-contain-at-least-eight-characters-at-least-one-number-a
  if (regexPassword.test(pass)) {
    showMessage(idError, ``);
    return true;
  } else {
    showMessage(idError, message);
    return false;
  }
}

function checkDate(date, idError, message) {
  let regexDate =
    /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2 68][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
  if (regexDate.test(date)) {
    showMessage(idError, ``);
    return true;
  } else {
    showMessage(idError, message);
    return false;
  }
}

function checkIncome(income, idError, message) {
  if (income >= 1e6 && income <= 20e6) {
    showMessage(idError, ``);
    return true;
  } else {
    showMessage(idError, message);
    return false;
  }
}

function checkPosition(pos, idError, message) {
  if (pos != `Chọn chức vụ`) {
    showMessage(idError, ``);
    return true;
  } else {
    showMessage(idError, message);
    return false;
  }
}

function checkHour(hour, idError, message) {
  if (hour >= 80 && hour <= 200) {
    showMessage(idError, ``);
    return true;
  } else {
    showMessage(idError, message);
    return false;
  }
}
