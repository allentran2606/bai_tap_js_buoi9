function NhanVien(
  accountNV,
  nameNV,
  mailNV,
  passwordNV,
  datepickerNV,
  incomeNV,
  positionNV,
  workingTimeNV
) {
  this.account = accountNV;
  this.name = nameNV;
  this.email = mailNV;
  this.passWord = passwordNV;
  this.date = datepickerNV;
  this.incomeCB = incomeNV;
  this.position = positionNV;
  this.time = workingTimeNV;
  this.totalIncome = function () {
    if (this.position == `Sếp`) {
      return this.incomeCB * 3;
    } else if (this.position == `Trưởng phòng`) {
      return this.incomeCB * 2;
    } else return this.incomeCB;
  };
  this.ranking = function () {
    if (this.time >= 192) {
      return `Nhân viên xuất sắc`;
    } else if (this.time >= 176) {
      return `Nhân viên giỏi`;
    } else if (this.time >= 160) {
      return `Nhân viên khá`;
    } else return `Nhân viên trung bình`;
  };
}
